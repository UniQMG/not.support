(() => {
  const canvas = document.getElementById("snow");
  const ctx = canvas.getContext('2d');
  let width, height;

  let snow = [];
  let particleCount = 50;

  let wind = { x: Math.random() * 12 - 6, y: -3 };
  let airResistance = 0.95; // as scalar of current speed per frame
  let variance = 0.01; // as max scalar of wind speed
  let gravity = 9.81;
  let pixelScale = 0.005;

  let r = 255;
  let g = 255;
  let b = 255;

  let ageLimit = 3600;
  let fadeTime = 600;

  let mouseX = -width;
  let mouseY = -height;

  document.addEventListener('mousemove', evt => {
    mouseX = evt.clientX;
    mouseY = evt.clientY;
  });

  let tx = 0;
  setInterval(() => {
    if (Math.abs(wind.x - tx) < 0.01)
      tx = Math.random() * 12 - 6;
    wind.x += (tx - wind.x) * 0.01;
  }, 100);

  let margin = 100; // Off-screen margin before particles wrap

  function refit() {
    let bcr = canvas.getBoundingClientRect();
    canvas.width = bcr.width;
    canvas.height = bcr.height;
    let oldWidth = width;
    let oldHeight = height;
    width = bcr.width;
    height = bcr.height;

    ctx.shadowBlur = 3.0;
    ctx.shadowColor = `rgba(${r}, ${g}, ${b}, 1)`;

    if (!isNaN(oldWidth) && !isNaN(oldHeight)) {
      snow.forEach(particle => {
        particle.x /= oldWidth;
        particle.y /= oldHeight;
        particle.x *= width;
        particle.y *= height;
      });
    }
  }

  window.addEventListener('resize', refit);
  refit();

  while (snow.length < particleCount) {
    snow.push(particle = {
      x: Math.random() * (width + margin*2),
      y: Math.random() * (height + margin*2),
      age: Math.random() * -fadeTime,
      alpha: 0,
      dx: 0,
      dy: 0
    });
  }

  let radius = 3;
  let pathLength = 50;
  let trailGrad = ctx.createRadialGradient(0, 0, 0, 0, 0, pathLength);
  trailGrad.addColorStop(0, `rgba(${r}, ${g}, ${b}, 1)`);
  trailGrad.addColorStop(0.75, `rgba(${r}, ${g}, ${b}, 0)`);

  function render() {
    for (i = snow.length-1; i >= 0; i--) {
      let particle = snow[i];
      particle.x += particle.dx;
      particle.y += particle.dy;

      let dis = (
        Math.pow(mouseX - particle.x, 2) +
        Math.pow(mouseY - particle.y, 2)
      );
      let ang = Math.atan2(particle.y - mouseX, particle.x - mouseX);
      let force = Math.min(200/dis, 10);
      if (dis < 100*100) {
        particle.dx += Math.cos(ang) * force;
        //particle.dy += Math.sin(ang) * force;
      }

      let mWidth = width + margin*2;
      let mHeight = height + margin*2;
      particle.x = (particle.x + mWidth*2) % mWidth;
      particle.y = (particle.y + mHeight*2) % mHeight;

      particle.dx += wind.x * pixelScale;
      particle.dy += (wind.y + gravity) * pixelScale;

      particle.dx += wind.x * variance * (Math.random()*2-1) * pixelScale;
      particle.dy += wind.y * variance * (Math.random()*2-1) * pixelScale;

      particle.dx *= airResistance;
      particle.dy *= airResistance;

      particle.age++;
      particle.alpha = Math.max(0, Math.min(1, particle.age/fadeTime));
    }

    ctx.clearRect(0, 0, width, height);

    ctx.fillStyle = trailGrad;
    for (i = snow.length-1; i >= 0; i--) {
      let particle = snow[i];
      if (particle.alpha < 0.01) continue;
      ctx.globalAlpha = particle.alpha;

      let vel = Math.sqrt(particle.dx*particle.dx + particle.dy*particle.dy);
      let pathLength = vel * 2;
      ctx.save();
      ctx.translate(particle.x, particle.y);
      ctx.rotate(Math.atan2(
        particle.dy + wind.y*pixelScale,
        particle.dx + wind.x*pixelScale
      ) - Math.PI/2);
      ctx.beginPath();
      ctx.moveTo(radius, -pathLength);
      ctx.lineTo(radius, 0);
      ctx.arc(0, 0, radius, 0, Math.PI);
      ctx.lineTo(-radius, -pathLength);
      ctx.arc(0, -pathLength, radius, Math.PI, 2*Math.PI);
      ctx.fill();
      ctx.restore();
    }

    ctx.fillStyle = `rgba(${r}, ${g}, ${b}, 1)`;
    for (i = snow.length-1; i >= 0; i--) {
      let particle = snow[i];
      if (particle.alpha < 0.01) continue;
      ctx.globalAlpha = particle.alpha;
      ctx.save();
      ctx.translate(particle.x, particle.y);
      ctx.beginPath();
      ctx.arc(0, 0, radius, 0, 2*Math.PI);
      ctx.fill();
      ctx.restore();
    }
    requestAnimationFrame(render);
  }
  render();
})();
