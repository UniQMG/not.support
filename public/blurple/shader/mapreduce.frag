/**
 * This shader determines the minimum local brightness
 * It's run multiple times, halving the canvas size each time
 * and feeding the result back into iteslf until the canvas reaches
 * a single pixel in size.
 *
 * Thanks to Rina#9973 for the idea to adjust brightness
 */

precision highp float;
uniform sampler2D texture;
varying vec2 vPos;

uniform int texWidth;
uniform int texHeight;

uniform bool useMaximum;

float toGrayscale(vec4 color) {
  float gs = dot(color.rgb, vec3(0.299, 0.587, 0.114));
  gs += (1.0 - color.a); /* Not actual grayscale but needed for our purposes */
  return gs;
}

/* Shorthand decider for min/max depending on useMaximum */
float mOp(float v1, float v2) {
  return useMaximum ? max(v1, v2) : min(v1, v2);
}

void main() {
  vec2 uv = vec2((vPos.x + 1.0)/2.0, 1.0 - (vPos.y + 1.0)/2.0);

  float dw = 1.0/float(texWidth);
  float dh = 1.0/float(texHeight);
  float grayscale = useMaximum ? 0.0 : 1.0;

  // Find the minimum surrounding grayscale value
  // The image is then downscaled and run again
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv)));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2( dw, 0.0))));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2(0.0,  dh))));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2( dw,  dh))));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2(-dw,  dh))));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2( dw, -dh))));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2(-dw, 0.0))));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2(0.0, -dh))));
  grayscale = mOp(grayscale, toGrayscale(texture2D(texture, uv + vec2(-dw, -dh))));

  gl_FragColor = vec4(grayscale, grayscale, grayscale, 1.0);
}
