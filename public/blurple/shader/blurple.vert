attribute vec2 vertex;
varying vec2 vPos;

void main() {
  vPos = vertex;
  gl_Position = vec4(vertex, 0.0, 1.0);
}
