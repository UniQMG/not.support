precision highp float;
uniform vec4 color;
varying vec2 vPos;

uniform sampler2D texture;

uniform vec3 colorStep1;
uniform vec3 colorStep2;
uniform vec3 colorStep3;

uniform float colorStage1;
uniform float colorStage2;

void main() {
  vec2 tPos = vec2((vPos.x + 1.0) / 2.0, 1.0 - (vPos.y + 1.0) / 2.0);
  vec4 color = texture2D(texture, tPos);
  float grayscale = dot(color.rgb, vec3(0.299, 0.587, 0.114));

  vec3 blurplified = vec3(0.0, 0.0, 0.0);

  /*
  vec3 colorStep1 = vec3(0.2, 0.0, 0.0);
  vec3 colorStep2 = vec3(0.5, 0.0, 0.0);
  vec3 colorStep3 = vec3(1.0, 0.0, 0.0);
  float colorStage1 = 0.33;
  float colorStage2 = 0.66;*/

  if (grayscale < colorStage1) {
    blurplified = colorStep1;
  } else if (grayscale < colorStage2) {
    blurplified = colorStep2;
  } else {
    blurplified = colorStep3;
  }

  gl_FragColor = vec4(blurplified, color.a);
  //gl_FragColor = vec4((vPos.x+1.0)/2.0, 0.0, 0.0, 1.0);
}
