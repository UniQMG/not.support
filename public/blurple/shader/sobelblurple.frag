precision highp float;
uniform vec4 color;
varying vec2 vPos;

uniform sampler2D texture;

uniform vec3 colorStep1;
uniform vec3 colorStep2;
uniform vec3 colorStep3;

uniform float colorStage1;
uniform float colorStage2;

uniform int texWidth;
uniform int texHeight;
uniform float minBrightness;
uniform float maxBrightness;

uniform bool edgeDetect;
uniform bool brightnessCorrection;
uniform bool compressRange;
uniform bool clobberAlpha;
uniform bool gradientMode;
uniform bool semicircleMode;
uniform bool rainbowMode;

vec4 makeSobel(vec2 uv);
vec3 blurplefy(vec3 color);
vec3 quantize(vec3 color);
vec3 hsv2rgb(vec3 color);

void main() {
  vec2 tPos = vec2((vPos.x + 1.0) / 2.0, 1.0 - (vPos.y + 1.0) / 2.0);
  vec4 color = texture2D(texture, tPos);

  vec4 sobel = edgeDetect ? makeSobel(tPos)*0.8 : color;
  vec3 blurplefied = blurplefy(max(color.rgb, sobel.rgb));
  float alpha = color.a;

  if (!clobberAlpha && color.a == 0.0) discard;

  gl_FragColor = vec4(blurplefied, clobberAlpha ? 1.0 : alpha);
}

vec3 quantize(vec3 color) {
  float grayscale = (color.r + color.g + color.b)/3.0;
  if (grayscale < colorStage1) {
    return colorStep1;
  } else if (grayscale < colorStage2) {
    return colorStep2;
  } else {
    return colorStep3;
  }
}

vec3 blurplefy(vec3 color) {
  float grayscale = dot(color, vec3(0.299, 0.587, 0.114));

  if (brightnessCorrection) {
    float range = maxBrightness - minBrightness;
    grayscale = (grayscale - minBrightness) / range;
  }

  if (compressRange) {
    grayscale = sqrt(grayscale);
  }

  if (gradientMode) {

    float scalar = semicircleMode
      ? sqrt(1.0 - (grayscale - 1.0)*(grayscale - 1.0))
      : grayscale;

    scalar = min(max(scalar, 0.0), 1.0);
    float mid = (colorStage2 + colorStage1)/2.0;

    if (rainbowMode) {
      float weight = 1.0 - abs(scalar - mid);
      float shift = 0.5 - mid;
      scalar += weight * shift;
      return hsv2rgb(vec3(1.0-scalar, 0.75, 0.75));
    }

    if (scalar <= mid) {
      float ratio = scalar / mid;
      return mix(colorStep1, colorStep2, ratio);
    } else {
      float ratio = (scalar - mid)/(1.0 - mid);
      return mix(colorStep2, colorStep3, ratio);
    }
  }

  return quantize(vec3(grayscale));
}

// see https://github.com/hughsk/glsl-hsv2rgb/blob/master/index.glsl
vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// see https://gist.github.com/Hebali/6ebfc66106459aacee6a9fac029d0115
vec4 makeSobel(vec2 coord) {
    vec4 n[9];
  	float w = 1.0 / float(texWidth);
  	float h = 1.0 / float(texHeight);
  	n[0] = texture2D(texture, coord + vec2( -w, -h));
  	n[1] = texture2D(texture, coord + vec2(0.0, -h));
  	n[2] = texture2D(texture, coord + vec2(  w, -h));
  	n[3] = texture2D(texture, coord + vec2( -w, 0.0));
  	n[4] = texture2D(texture, coord);
  	n[5] = texture2D(texture, coord + vec2(  w, 0.0));
  	n[6] = texture2D(texture, coord + vec2( -w, h));
  	n[7] = texture2D(texture, coord + vec2(0.0, h));
  	n[8] = texture2D(texture, coord + vec2(  w, h));
    vec4 sobel_edge_h = n[2] + (2.0*n[5]) + n[8] - (n[0] + (2.0*n[3]) + n[6]);
    vec4 sobel_edge_v = n[0] + (2.0*n[1]) + n[2] - (n[6] + (2.0*n[7]) + n[8]);
    return sqrt((sobel_edge_h * sobel_edge_h) + (sobel_edge_v * sobel_edge_v));
}
