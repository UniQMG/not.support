/* © UniQMG */
(async () => {
  const canvas = document.getElementById('blurplecanvas');
  const dlAnchor = document.getElementById('blurpledownloadanchor');
  const drawAll = document.getElementById('drawall');
  const animate = document.getElementById('animate');
  const framerate = document.getElementById('framerate');
  const clobberAlpha = document.getElementById('clobberalpha');
  const enableTransparency = document.getElementById('enabletransparency');
  const gl = canvas.getContext('webgl', { premultipliedAlpha: false });
  var frameData = []; // gif frame data
  var lastFrame = 0; // The last rendered frame index


  const secretButton = document.getElementById('rgb');
  let secretMode = false;
  if (secretButton) {
    secretButton.addEventListener('click', async evt => {
      evt.preventDefault();
      secretMode = true;
      dlAnchor.click();
    });
  }

  function ahrefDownload(url, fileext) {
    console.log("Output URL: ", url);
    console.log("If it didn't download correctly, copy and paste the previous");
    console.log("URL into your browser and download that. If it starts with");
    console.log("\"blob:http://\", include the \"blob:\"!");

    let outputImages = document.getElementById('output-images');

    let container = document.createElement('div');
    outputImages.appendChild(container);
    container.classList.add('output-image');

    let a = document.createElement('a');
    a.setAttribute("download", "blurple." + fileext);
    a.innerText = "re-download";
    a.href = url;
    container.appendChild(a);

    a.appendChild(document.createElement('br'));

    let img = new Image();
    img.src = url;
    a.appendChild(img);

    a.dispatchEvent(new MouseEvent(`click`, {
      bubbles: true, cancelable: true, view: window
    }));
  }

  let ignoreNextClick = false;
  dlAnchor.addEventListener('click', async evt => {
    evt.preventDefault();
    if (ignoreNextClick) {
      ignoreNextClick = false;
      return;
    }

    gl.preserveDrawingBuffer = true;
    let animateStatus = animate.checked;
    animate.checked = false;

    if (mode == 'video') {
      const encoder = new Whammy.Video(framerate.value);

      let frameTime = 1000 / framerate.value;
      let frameCount = Math.floor(video.duration*1000 / frameTime);
      for (let i = 0; i < frameCount; i++) {
        setStatus(`Drawing video (${i}/${frameCount})`);
        await new Promise(res => setTimeout(res));

        video.currentTime = i * frameTime/1000;
        await new Promise(res => video.onseeked = res);
        loadVideoFrame();
        draw(0);
        gl.flush();
        encoder.add(canvas.toDataURL('image/webp', 0.8));
      }

      setStatus(`Encoding video`);
      encoder.compile(false, blob => {
        ahrefDownload(URL.createObjectURL(blob), 'webm');
        setStatus('Idle', true);
      });
    } else if (frameData.length > 1) {
      const gif = new GIF({
        background: colorPickers[0].get().hex(),
        workers: navigator.hardwareConcurrency || 1,
        transparent: enableTransparency.checked
          ? 'rgba(0.0,0.0,0.0,0.0)'
          : undefined,
        quality: 10
      });

      for (let i = 0; i < frameData.length; i++) {
        setStatus(`Drawing GIF (${i}/${frameData.length})`)
        let { delay, disposal } = frameData[i].frameInfo;
        draw(i);
        gl.flush();
        gl.finish();
        gif.addFrame(canvas, {
          copy: true,
          delay: delay*10,
          dispose: disposal
        });
      }

      setStatus('Rendering GIF');
      gif.on('finished', blob => {
        ahrefDownload(URL.createObjectURL(blob), 'gif');
        setStatus('Idle', true);
      });
      gif.render();
    } else if (secretMode) {
      secretMode = false;
      const gif = new GIF({
        background: colorPickers[0].get().hex(),
        workers: navigator.hardwareConcurrency || 1,
        quality: 100
      });

      for (let i = 0; i < 360; i += 2) {
        colorPickers[1].set(`hsl(${i}, 100%, 72.5%)`);
        draw(0);
        gl.flush();
        gl.finish();
        gif.addFrame(canvas, {
          copy: true,
          delay: 40
        });
        setStatus(`${i} / 360`);
        await new Promise(res => setTimeout(res));
      }

      setStatus('Rendering');
      gif.on('finished', blob => {
        ahrefDownload(URL.createObjectURL(blob), 'gif');
        setStatus('Idle', true);
      });
      gif.render();
    } else {
      setStatus('Drawing');
      draw();
      gl.flush();
      gl.finish();
      ahrefDownload(canvas.toDataURL("image/png"), 'png');
      setStatus('Idle', true);
    }

    gl.preserveDrawingBuffer = false;
    animate.checked = animateStatus;
  });

  if (gl === null) {
    alert("Your browser doesn't support WebGL.");
    return;
  }

    const statusElem = document.getElementById('status');
    function setStatus(status, isIdle) {
      statusElem.innerText = status;
      statusElem.classList.toggle('idle', !!isIdle);
    }

  const vertShaderSrc = await (await fetch('shader/blurple.vert')).text();
  const fragShaderSrc = await (await fetch('shader/sobelblurple.frag')).text();
  const mapReduceShaderSrc = await (await fetch('shader/mapreduce.frag')).text();

  const program = gl.createProgram();
  const mpProgram = gl.createProgram(); // mapreduce

  const vertShader = gl.createShader(gl.VERTEX_SHADER);
  const fragShader = gl.createShader(gl.FRAGMENT_SHADER);
  const mapReduceShader = gl.createShader(gl.FRAGMENT_SHADER);

  gl.shaderSource(vertShader, vertShaderSrc);
  gl.shaderSource(fragShader, fragShaderSrc);
  gl.shaderSource(mapReduceShader, mapReduceShaderSrc);

  gl.compileShader(vertShader);
  gl.compileShader(fragShader);
  gl.compileShader(mapReduceShader);

  gl.attachShader(program, vertShader);
  gl.attachShader(program, fragShader);
  gl.linkProgram(program);

  gl.attachShader(mpProgram, vertShader);
  gl.attachShader(mpProgram, mapReduceShader);
  gl.linkProgram(mpProgram);

  console.log("Vertex shader compile out:", gl.getShaderInfoLog(vertShader));
  console.log("Fragment shader compile out:", gl.getShaderInfoLog(fragShader));
  console.log("Map-reduce shader compile out:", gl.getShaderInfoLog(mapReduceShader));
  console.log("Program compile out:", gl.getProgramInfoLog(program));
  console.log("mpProgram compile out:", gl.getProgramInfoLog(mpProgram));

  const vertexElements = 2;
  const vertices = new Float32Array([
    -1, -1,
     1, -1,
    -1,  1,

     1, -1,
     1,  1,
    -1,  1
  ]);

  /* Single texture, used for preprocessing */
  const texture = gl.createTexture();

  /* Textures for gif frames */
  var frames = [];
  var textures = [];
  function getTexture(i) {
    if (!textures[i]) textures[i] = gl.createTexture();
    return textures[i];
  }

  const level = 0;
  const internalFormat = gl.RGBA;
  const srcFormat = gl.RGBA;
  const srcType = gl.UNSIGNED_BYTE;
  let texImageArgs = [gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType];

  const reducedTexture = gl.createTexture();
  let widthLoc = gl.getUniformLocation(program, 'texWidth');
  let heightLoc = gl.getUniformLocation(program, 'texHeight');
  let minBrightnessLoc = gl.getUniformLocation(program, 'minBrightness');
  let maxBrightnessLoc = gl.getUniformLocation(program, 'maxBrightness');
  let mpWidthLoc = gl.getUniformLocation(mpProgram, 'texWidth');
  let mpHeightLoc = gl.getUniformLocation(mpProgram, 'texHeight');
  let mpUseMaximumLoc = gl.getUniformLocation(mpProgram, 'useMaximum');

  function toggleVisibleOptions(mode) {
    let gifElems = [...document.getElementsByClassName('onlygif')];
    let videoElems = [...document.getElementsByClassName('onlyvideo')];
    let imageElems = [...document.getElementsByClassName('onlyimage')];
    let gifCombineElems = [...document.getElementsByClassName('onlycombinegif')];
    [...videoElems, ...gifElems, ...gifCombineElems, ...imageElems].forEach(elem => {
      elem.style.display = 'none';
    });
    console.log("UI to mode", mode);

    switch (mode) {
      case 'image':
        imageElems.forEach(elem => {
          elem.style.display = 'inherit';
        });
        break;

      case 'video':
        [...gifElems, ...videoElems].forEach(elem => {
          elem.style.display = 'inherit';
        });
        break;

      case 'gif':
        gifElems.forEach(elem => {
          elem.style.display = 'inherit';
        });
        break;

      case 'combinegif':
        [...gifElems, ...gifCombineElems].forEach(elem => {
          elem.style.display = 'inherit';
        });
        break;
    }
  }
  function updateImage(image) {
    return new Promise((resolve, reject) => {
      setStatus("Loading image");

      image.onload = (async () => {
        frames = [];
        textures = [];
        frameData = [];
        lastFrame = 0;

        if (image.src.endsWith('.gif') ||  (image.originalSrc && image.originalSrc.endsWith('.gif'))) {
          canvas.classList.toggle('gif', true);
          toggleVisibleOptions('gif');

          let start = performance.now();
          setStatus("Decoding GIF");
          frameData = await gifFrames({
            url: image.src,
            frames: 'all',
            outputType: 'canvas'
          });

          console.log("Disposal mode", frameData[0].frameInfo.disposal)
          if (frameData[0].frameInfo.disposal === 1)
            toggleVisibleOptions('combinegif');

          setStatus("Drawing GIF to buffers");
          await new Promise(res => setTimeout(res));

          frames = await Promise.all(frameData.map(frame => {
            let img = new Image();
            img.src = frame.getImage().toDataURL();
            return new Promise(res => img.onload = () => res(img));
          }));

          let length = Math.floor((performance.now() - start)*100)/100;
          console.log(`Decoded ${frames.length} frames in ${length}ms.`);
          setStatus("Done", true);
        } else {
          canvas.classList.toggle('gif', false);
          toggleVisibleOptions('image');
          frames = [image];
        }

        image = frames[0];
        let reduceImageMin = frames[0];
        let reduceImageMax = frames[0];

        gl.preserveDrawingBuffer = true;
        canvas.style.width = '60vw';
        canvas.style.height = image.height/image.widtsh * 60 + 'vw';
        let start = performance.now();
        let step = 0;
        let steps = Math.floor(Math.log2(image.width)-1);
        canvas.width = image.width;
        canvas.height = image.height;

        while (canvas.width > 2) {
          step++;
          setStatus(`Preprocessing (${step+1}/${steps})`)
          canvas.width = Math.floor(canvas.width/2);
          canvas.height = Math.ceil(canvas.width/image.width * image.height);
          gl.useProgram(mpProgram);
          gl.uniform1i(mpWidthLoc, canvas.width);
          gl.uniform1i(mpHeightLoc, canvas.height);

          gl.activeTexture(gl.TEXTURE0);
          gl.bindTexture(gl.TEXTURE_2D, reducedTexture);
          gl.texImage2D(...texImageArgs, reduceImageMax);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

          gl.uniform1i(mpUseMaximumLoc, true);
          render(gl, mpProgram, canvas);
          reduceImageMax = new Image();
          reduceImageMax.src = canvas.toDataURL();

          gl.uniform1i(mpUseMaximumLoc, false);
          gl.texImage2D(...texImageArgs, reduceImageMin);
          render(gl, mpProgram, canvas);
          reduceImageMin = new Image();
          reduceImageMin.src = canvas.toDataURL();

          await Promise.all([
            new Promise(res => reduceImageMax.onload = res),
            new Promise(res => reduceImageMin.onload = res)
          ]);
        }
        gl.preserveDrawingBuffer = false;
        canvas.style.width = 'unset';
        canvas.style.height = 'unset';

        let outcanvas = document.createElement('canvas');
        let ctx = outcanvas.getContext('2d');
        outcanvas.width = 1;
        outcanvas.height = 1;

        ctx.drawImage(reduceImageMin, 0, 0);
        var pixels = ctx.getImageData(0, 0, 1, 1).data;
        let minimumBrightness = pixels[0] / 255;

        ctx.drawImage(reduceImageMax, 0, 0);
        var pixels = ctx.getImageData(0, 0, 1, 1).data;
        let maximumBrightness = pixels[0] / 255;

        let time = Math.floor((performance.now() - start)*100)/100;
        console.log(`Brightness clamp: ${minimumBrightness} - ${maximumBrightness} (took ${time}ms)`);

        let brightness = document.getElementById('brightness');
        let minBr = Math.floor(minimumBrightness*100);
        let maxBr = Math.floor(maximumBrightness*100);
        brightness.innerText = `${minBr}% - ${maxBr}%`;

        canvas.width = image.width;
        canvas.height = image.height;
        gl.useProgram(program);
        gl.uniform1f(minBrightnessLoc, minimumBrightness);
        gl.uniform1f(maxBrightnessLoc, maximumBrightness);
        gl.uniform1i(widthLoc, image.width);
        gl.uniform1i(heightLoc, image.height);
        for (var i = 0; i < frames.length; i++) {
          gl.activeTexture(gl.TEXTURE0);
          gl.bindTexture(gl.TEXTURE_2D, getTexture(i));
          gl.texImage2D(...texImageArgs, frames[i]);

          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        }

        drawAnimation();
        draw(0);
        resolve();
      });
      image.onerror = reject;
    });
  }

  var image = new Image();
  var video = document.createElement('video');
  window.image = image;
  window.video = video;

  let mode = 'image';
  image.src = 'img/example.jpg';

  const upload = document.getElementById("upload");
  upload.addEventListener('change', data => {
    setTimeout(() => upload.value = "");
    [...data.target.files].forEach(file => {
      var reader = new FileReader();
      reader.onload = async function(data) {
        console.log("File name", file.name);
        switch (file.name.split('.').slice(-1)[0]) {
          case 'webm': case 'mp4': case 'ogg':
            mode = 'video';
            video = document.createElement('video');
            video.src = data.target.result;
            console.log("DID SOMEONE SAY WEBM???");
            toggleVisibleOptions('video');
            video.load();
            await new Promise(res => video.onloadeddata = res);
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            loadVideoFrame();
            draw();
            frameData.length = 0;
            break;

          default:
            mode = 'image';
            let image = new Image();
            image.originalSrc = file.name;
            image.src = data.target.result;
            updateImage(image).then(draw.bind(null, 0));
            break;
        }
      };
      reader.readAsDataURL(file);
    });
  });

  gl.useProgram(program);

  function createFlag(checkboxId, variableName) {
    let variableLoc = gl.getUniformLocation(program, variableName);
    const checkbox = document.getElementById(checkboxId);
    checkbox.addEventListener('change', data => {
      console.log(checkboxId, '=>', variableName, ':', checkbox.checked);
      gl.uniform1i(variableLoc, checkbox.checked);
      draw();
    });
    gl.uniform1i(variableLoc, checkbox.checked);
  }

  createFlag('brightnesscorrect', 'brightnessCorrection');
  createFlag('semicirclemode', 'semicircleMode');
  createFlag('compressrange', 'compressRange');
  createFlag('gradientmode', 'gradientMode');
  createFlag('clobberalpha', 'clobberAlpha');
  createFlag('rainbowmode', 'rainbowMode');
  createFlag('edgedetect', 'edgeDetect');
  createFlag('flipyaxis', 'flipY');

  const textureCoords = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, textureCoords);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    0, 0,   1, 0,   1, 1,   0, 1
  ]), gl.STATIC_DRAW);

  const vertexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  const vertexLoc = gl.getUniformLocation(program, "vertex");
  gl.enableVertexAttribArray(vertexLoc);
  gl.vertexAttribPointer(vertexLoc, vertexElements, gl.FLOAT, false, 0, 0);

  const colorLocations = [
    gl.getUniformLocation(program, 'colorStep1'),
    gl.getUniformLocation(program, 'colorStep2'),
    gl.getUniformLocation(program, 'colorStep3')
  ];
  const stageLocations = [
    gl.getUniformLocation(program, 'colorStage1'),
    gl.getUniformLocation(program, 'colorStage2')
  ];

  const stageSelectors = [
    document.getElementById("colorStage1"),
    document.getElementById("colorStage2")
  ];

  stageSelectors[0].oninput = draw.bind(null, null);
  stageSelectors[1].oninput = draw.bind(null, null);

  let options = [
    'currentColor',
    'hex',
    ['fields', {space: 'RGB', limit: 255, fix: 0}],
    ['fields', {space: 'HSL', limit: 255, fix: 0}],
  ];

  const defaultColors = ['#4e5d94', '#7289da', '#FFFFFF'];
  if (window.overrideColors)
    window.overrideColors(defaultColors);

  const colorPickers = [
    colorjoe.rgb('colorStep1', defaultColors[0], options),
    colorjoe.rgb('colorStep2', defaultColors[1], options),
    colorjoe.rgb('colorStep3', defaultColors[2], options)
  ];
  window.colorPickers = colorPickers;

  const colors = [];
  colorPickers.forEach((joe, i) => {
    joe.on('change', onecolor => {
      let color = onecolor.rgb();
      colors[i] = [ color._red, color._green, color._blue ];
      draw();
    }).update();
  });
  document.getElementById('swapcolors').onclick = ((evt) => {
    let c1 = colorPickers[0].get();
    let c2 = colorPickers[colorPickers.length-1].get();
    colorPickers[colorPickers.length-1].set(c1);
    colorPickers[0].set(c2);
    draw();
  });
  document.getElementById('resetsliders').onclick = ((evt) => {
    stageSelectors[0].value = 33;
    stageSelectors[1].value = 66;
    draw();
  });
  // bluplre/white logos and blurple/not quite black logos
  document.getElementById('setlighttheme').onclick = ((evt) => {
    colorPickers[0].set('#4E5D94');
    colorPickers[1].set('#7289DA');
    colorPickers[2].set('#FFFFFF');
    draw();
  });
  document.getElementById('setdarktheme').onclick = ((evt) => {
    colorPickers[0].set('#23272A');
    colorPickers[1].set('#4E5D94');
    colorPickers[2].set('#7289DA');
    draw();
  });
  document.getElementById('setorangered').onclick = ((evt) => {
    colorPickers[0].set('#B6522D');
    colorPickers[1].set('#FF4500');
    colorPickers[2].set('#FFFFFF');
    draw();
  });



  await updateImage(image);

  function render(gl, program, canvas) {
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.useProgram(program);

    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.drawArrays(gl.TRIANGLES, 0, vertices.length / vertexElements);
  }
  function draw(frame) {
    if (!frame && frame != 0) frame = lastFrame;
    lastFrame = frame;

    if (!image.complete) return;
    let start = performance.now();
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.useProgram(program);

    colors.forEach((color, i) => {
      gl.uniform3fv(colorLocations[i], color);
    });
    stageSelectors.forEach((val, i) => {
      gl.uniform1f(stageLocations[i], val.value/100);
    });

    gl.activeTexture(gl.TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);

    if (mode == 'image' && drawAll.checked && frameData[0] && frameData[0].frameInfo.disposal === 1) {
      for (var fi = 0; fi <= frame; fi++) {
        gl.bindTexture(gl.TEXTURE_2D, getTexture(fi));
        gl.drawArrays(gl.TRIANGLES, 0, vertices.length / vertexElements);
      }
    } else {
      gl.bindTexture(gl.TEXTURE_2D, mode == 'video' ? getTexture('video') : getTexture(frame));
      gl.drawArrays(gl.TRIANGLES, 0, vertices.length / vertexElements);
    }

    let time = Math.floor((performance.now() - start)*1000)/1000;
    setStatus(`Idle (Last draw: ${time}ms)`, true);
  };

  animate.onchange = function() {
    gl.preserveDrawingBuffer = false;
    drawAnimation();
  }
  window.draw = draw;

  function loadVideoFrame() {
    let videoTexture = getTexture('video');
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, videoTexture);
    gl.texImage2D(...texImageArgs, video);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  }

  async function drawAnimation() {
    if (mode == 'image') {
      if (animate.checked && !frameData.length)
        return setTimeout(drawAnimation, 100);
      if (!animate.checked) return draw(lastFrame);
      for (var i = 0; i < frames.length; i++) {
        if (!animate.checked) return;
        draw(i);

        await new Promise(res => {
          if (!frameData[i]) res();
          setTimeout(res, frameData[i].frameInfo.delay*10)
        });
      }
      if (animate.checked) setTimeout(drawAnimation);
    } else {
      while (true) {
        if (!animate.checked) return;

        let last = Date.now();
        let resolved = false;
        let delay = 1000 / (framerate.value || 30);
        let delayPromise = new Promise(res => setTimeout(() => {
          resolved = true;
          res();
        }, delay));

        loadVideoFrame();

        await delayPromise;
        video.currentTime = video.currentTime + (Date.now() - last)/1000;
        if (video.currentTime >= video.duration)
          video.currentTime = 0;
        await new Promise(res => video.onseeked = res);

        draw(0);
      }
    }
  }
})();
